// author: Gegham Vardanyan
// hw: hw2 - pythono mudule lua.so
// date: 21.03.2016
extern "C" {
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <Python.h>
#include <string.h>
#include <stdlib.h>
}
#include <string>
#include <iostream>

PyObject* GetPyVal(lua_State* L) {
 //   std::cout << "GetPyVal" << std::endl;
    PyObject* pyRes;

    if (lua_isboolean(L, -1)) {

 //       std::cout << "is boolean" << std::endl;
        bool res = lua_toboolean(L, -1);
       // pyRes = PyBool_FromLong(res);
        pyRes = Py_BuildValue("i", res);

    } else if (lua_isinteger(L, -1)) {

 //       std::cout << "is integer" << std::endl;
        long long res = lua_tointeger(L, -1);
        pyRes = Py_BuildValue("i", res);

    } else if (lua_isnumber(L, -1)) {

 //       std::cout << "is number1" << std::endl;
        double res = lua_tonumber(L, -1);
        //pyRes = PyFloat_FromDouble(res);
        pyRes = Py_BuildValue("d", res);

    } else if (lua_isstring(L, -1)) {

 //       std::cout << "is string" << std::endl;
        const char* res = lua_tostring(L, -1);
        //pyRes = PyUnicode_FromString(res);
        pyRes = Py_BuildValue("s", res);

    } else if(lua_isnil(L, -1)) {

//        std::cout << "is nil" << std::endl;
        Py_INCREF(Py_None);
        pyRes = Py_None;

    }

    return pyRes;
}


void PassVarToLua(PyObject* dict, lua_State* L) {
    PyObject *key, *val;
    Py_ssize_t pos = 0;

    while (PyDict_Next(dict, &pos, &key, &val)) {
        if (PyBool_Check(val)) {

            bool cVal;
            if (val == Py_True) {
                cVal = true;
            } else {
                cVal = false;
            }
  //          std::cout << "cVal : " << cVal << std::endl;
            lua_pushboolean(L, cVal);


        } else if (PyLong_Check(val)) {

            long long cVal = PyLong_AsLong(val);
  //          std::cout << "cVal : " << cVal << std::endl;
            lua_pushinteger(L, cVal);

        } else if (PyFloat_Check(val)) {

            double cVal = PyFloat_AsDouble(val);
  //          std::cout << "cVal : " << cVal << std::endl;
            lua_pushnumber(L, cVal);

        } else if (PyUnicode_Check(val)) {

            const char* cVal = PyUnicode_AsUTF8(val);
   //         std::cout << "cVal : " << cVal << std::endl;
            lua_pushstring(L, cVal);

        } else {

            lua_pushnil(L);

        }
        
        const char* cKey = PyUnicode_AsUTF8(key);
        if(cKey[0] != '_'){
    //        std::cout << "CKey : " << cKey << std::endl;
            lua_setglobal(L, cKey);
        }
    }
}

void ChangeVar(PyObject* dict, lua_State* L) {
    PyObject *key, *val;
    Py_ssize_t pos = 0;
    while (PyDict_Next(dict, &pos, &key, &val)) {

        const char* cKey = PyUnicode_AsUTF8(key);
        lua_getglobal(L, cKey);

        if (lua_isboolean(L, -1)) {

            bool cVal = lua_toboolean(L, -1);
            val = PyBool_FromLong(cVal);

        } else if (lua_isinteger(L, -1)) {

            long long cVal = lua_tointeger(L, -1);
            val = PyLong_FromLong(cVal);

        } else if (lua_isnumber(L, -1)) {

            double cVal = lua_tonumber(L, -1);
            val = PyFloat_FromDouble(cVal);

        } else if (lua_isstring(L, -1)) {

            const char* cVal = lua_tostring(L, -1);
            val = PyUnicode_FromString(cVal);

        } else {

            val = Py_None;

        }

        PyDict_SetItem(dict, key, val);

        lua_pop(L, 1);
    }
}

static PyObject * lua_eval(PyObject* myModule, PyObject* args)
{
	PyObject* first;
	lua_State* myLua;

	first = PyTuple_GetItem(args, 0);

    std::string command(PyUnicode_AsUTF8(first));
    if (command.find('\n') == std::string::npos) {
        command = "return " + command;
    }
 //   std::cout << command << std::endl;

    myLua = luaL_newstate();
    luaL_openlibs(myLua);

// pass variables
    Py_ssize_t argSize = PyTuple_Size(args);
    if (argSize > 1) {

        PyObject* globals = PyTuple_GetItem(args, 1);
        if (globals == Py_None) {
  //          std::cout << "Py_None" << std::endl;
            PassVarToLua(PyEval_GetGlobals(), myLua);
  //          std::cout << "Py_None2" << std::endl;

        } else {

            PassVarToLua(globals, myLua);

        }
        if (argSize > 2) {

            PyObject* locals = PyTuple_GetItem(args, 2);
            PassVarToLua(locals, myLua);

        }
    }

// load code
    luaL_loadbuffer(myLua, command.c_str(), command.length(), NULL);

// exception
    if (lua_pcall(myLua, 0, 1, 0)) {
        const char* err = lua_tostring(myLua, -1);
        PyObject* exception = PyExc_BaseException;

        PyErr_SetString(exception, err);
        return exception;
    }

//change variables
    if (argSize > 1) {
        PyObject* globals = PyTuple_GetItem(args, 1);

        if (globals == Py_None) {

            PyObject* temp = PyEval_GetGlobals();
            ChangeVar(temp, myLua);

        } else {

            ChangeVar(globals, myLua);

        }
        if (argSize > 2) {

            PyObject* locals = PyTuple_GetItem(args, 2);
            ChangeVar(locals, myLua);

        }
    }

    PyObject* pyRes, temp;
    pyRes = GetPyVal(myLua);
    lua_close(myLua);
   
    if(pyRes == Py_None)
        Py_INCREF(Py_None);

   /* int i;
    const char* cTemp;
    PyObject* dict;
    if (!PyArg_ParseTuple(args, "sO!", &cTemp, &PyDict_Type, &dict)) {
        return NULL;
    }

    PyDict_SetItemString(dict, "first",
        Py_BuildValue("i", 8));
   
    return Py_BuildValue("O", dict);
    */
    return pyRes;
}


PyMODINIT_FUNC PyInit_lua(void)
{
    static PyMethodDef myModuleMethods[] = {
        { "eval", lua_eval, METH_VARARGS, "embedding eval function for Lua in c++" },
        { NULL, NULL, 0, NULL }
    };


    static struct PyModuleDef myModuleDef = {
        PyModuleDef_HEAD_INIT,
        "lua",
        "embedding eval function for Lua in c++",
            -1, myModuleMethods};


    return PyModule_Create(&myModuleDef);
}
