// author: Gegham Vardanyan
// hw: hw2 - pythono mudule lua.so
// date: 14.03.2016
extern "C" {
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <Python.h>
#include <string.h>
#include <stdlib.h>
}

static PyObject * lua_eval(PyObject* myModule, PyObject* args)
{
	PyObject* first;
	lua_State* myLua;
	first = PyTuple_GetItem(args, 0);
    const char* command = PyUnicode_AsUTF8(first);
    
    myLua = luaL_newstate();
    luaL_openlibs(myLua);
 // read lua code
    luaL_loadbuffer(myLua, command, strlen(command), NULL);
    lua_pcall(myLua, 0, 0, 0);
    lua_close(myLua);
    
    Py_INCREF(Py_None);
    return Py_None;
}

PyMODINIT_FUNC PyInit_lua(void)
{
    static PyMethodDef myModuleMethods[] = {
        { "eval", lua_eval, METH_VARARGS, "embedding eval function for Lua in c++" },
        { NULL, NULL, 0, NULL }
    };


    static struct PyModuleDef myModuleDef = {
        PyModuleDef_HEAD_INIT,
        "lua",
        "embedding eval function for Lua in c++",
            -1, myModuleMethods};


    return PyModule_Create(&myModuleDef);
}
