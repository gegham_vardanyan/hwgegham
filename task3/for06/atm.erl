%Autor: Gegham Vardanyan
-module(atm).
% this program for  0.6
-export([withdraw/2, withdraw/3]). 

withdraw(_Amount, []) ->
%  io:fwrite("5!~n", []),
  exit(normal);

withdraw(Amount, BNS) ->
% first step
%  io:fwrite("1!~n", []),
  withdraw(Amount, lists:reverse(lists:sort(BNS)), currentlyWorking).

withdraw(0, BNS, currentlyWorking) ->
%  io:fwrite("3!~n", []),
  {ok, [], BNS};

withdraw(_Amount, [], currentlyWorking) ->
%  io:fwrite("4!~n", []),
  {request_another_amount, [], []};

withdraw(Amount, BNS, currentlyWorking) ->
%  io:fwrite("2!~n", []),
  [CurBN|Rest] = BNS,

  {CurResult, CurRestBNS} =
    if
      CurBN>Amount ->
        CurAmount = 0,
%  io:fwrite("2.1!~n", []),
        {[], [CurBN]};
      true ->
% if equals, than we take it      
        CurAmount = CurBN,
%  io:fwrite("2.2!~n", []),      
        {[CurBN], []}
    end,

  {Status, Result, RestBNS} = withdraw(Amount - CurAmount, Rest, currentlyWorking),

  if
 % adding currents to previous   
    Status==ok -> {ok, CurResult ++ Result, CurRestBNS ++ RestBNS};
    Status==request_another_amount -> {request_another_amount, [], BNS}
  end.






