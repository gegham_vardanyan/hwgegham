% Autor: Gegham Vardanyan
-module(atm_sup).

-behaviour(supervisor).





-export([start_link/0]).


-export([init/1]).

% This is helper for children of supervisor
-define(ChilldOFSupervisor(S, TypeOfS), {S, {S, start_link, []}, permanent, 5000, TypeOfS, [S]}).



start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).




init([]) ->
    {ok, { {one_for_one, 10, 10},
        [
            ?ChilldOFSupervisor(atm_server, worker),
            ?ChilldOFSupervisor(transactions_server, worker)
        ]
    } }.
