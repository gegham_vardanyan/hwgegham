% Autor: Geham Vardanyan
-module(transactions_server).

-behaviour(gen_server).

-export([start_link/0, stop/0]).

% callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-define(SERVER, {global, transactions_server}).

start_link() ->
  gen_server:start_link(?SERVER, ?MODULE, [], []).

stop() ->
  gen_server:cast(?MODULE, stop).

init([]) ->
  dets:open_file(log, [{type, set}]),
  InitState =
  case dets:first(log) of
    '$end_of_table' -> [];
    history ->
      [{history, Log}] = dets:lookup(log, history),
      Log
  end,
  process_flag(trap_exit, true),
  {ok, InitState}.

handle_call(history, _, State) ->
  Result = State,
  NewState = State,
  {reply, Result, NewState};

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

handle_cast(stop, _State) ->
  dets:close(log),
  {stop, normal, _State} ;

handle_cast(clear, _State) ->
  dets:insert(log, {history, []}),
  {noreply, []};

handle_cast({withdraw, Amount}, State) ->
  NewState = lists:append(State, [Amount]),
  dets:insert(log, {history, NewState}),
  {noreply, NewState};

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info({'EXIT', _Pid, _Reason}, State) ->
  dets:close(log),
  {noreply, State};

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  dets:close(log),
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
