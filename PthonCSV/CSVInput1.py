import csv
import string
import sys
import math
import importlib.machinery
def main(argv):
    alpha = string.ascii_uppercase
    dic = {}
    for func in dir(math):
        if func[0] != '_':
                dic[func] = eval('math.'+func)
    if len(argv) == 4:
        file = argv[3]
        loader = importlib.machinery.SourceFileLoader(file,file)
        lib = loader.load_module(file)
        for func in dir(lib):
           if func[0] != '_':
                dic[func] = eval('lib' + '.' + func)


    path = argv[1]
    l = []
    for temp in range(1,3):
        with open(path, newline='') as f:
            reader = csv.reader(f, dialect = 'excel')
            i = 0
            for row in reader:
                i += 1
                j = 0
                l1 = []
                for x in row:
                    shift = 0
                    if x[0] == '"':
                        shift = 1
 #                   print('shift', shift, 'i', i, 'j', j, 'x', x, 'x[0]', x[0], 'x[shift]', x[shift])
                    j += 1
                    coord = alpha[j-1] + str(i)
                    if x[shift] != '=' and temp == 1:
                        try:                           
                            dic[coord] = float(x[shift:len(x) - shift])
                            if dic[coord] == int(dic[coord]):
                                dic[coord] = int(dic[coord])
                        except:
                            dic[coord] = str(x)
                    if x[shift] == '=':
                        strCell = x[shift + 1:len(x) - shift]
                        try:
                            dic[coord] = eval(strCell,dic)
                        except:
                            if temp == 2:
                                dic[coord] = "ERRORE"
                    if temp == 2:
                        if shift == 0:
                            l1.append(dic[coord])
                        else:
                            l1.append('"' + str(dic[coord]) + '"')
                            
                if temp == 2:
                    l.append(l1)
        f.close()
                        
    OutPath = argv[2]
    with open(OutPath, 'w', newline='') as csvfile:
        mywriter = csv.writer(csvfile, dialect = 'excel')
        for row in l:
            mywriter.writerow(row)
main(sys.argv)            

